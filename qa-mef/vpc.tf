provider "aws" {
  region = var.region
}
#######################VPC#################################
resource "aws_vpc" "qa-vpc" {
  cidr_block = var.vpc_cidr
  instance_tenancy = "default"
  enable_dns_hostnames = "true"

tags = {
  Name = "vpc-test-us2-qa"
  Location ="US"
}
}

#Subnet private

resource "aws_subnet" "subnet_private_1" {
  availability_zone = var.az1
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.10.0/24"

  tags = {
    Name = "subnet-test-us2a-qa-private"
  }
}

resource "aws_subnet" "subnet_private_2" {
  availability_zone = var.az2
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.11.0/24"


  tags = {
    Name = "subnet-test-us2b-qa-private"
  }
}

resource "aws_subnet" "subnet_private_3" {
  availability_zone = var.az3
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.12.0/24"

  tags = {
    Name = "subnet-test-us2c-qa-private"
  }
}

#Subnet public

resource "aws_subnet" "subnet_public_1" {
  availability_zone = var.az1
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.1.0/24"

  tags = {
    Name = "subnet-test-us2a-qa-public"
  }
}

resource "aws_subnet" "subnet_public_2" {
  availability_zone = var.az2
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.2.0/24"

  tags = {
    Name = "subnet-test-us2b-qa-public"
  }
}

resource "aws_subnet" "subnet_public_3" {
  availability_zone = var.az3
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.3.0/24"

  tags = {
    Name = "subnet-test-us2c-qa-public"
  }
}


#Subnet database

resource "aws_subnet" "subnet_database_1" {
  availability_zone = var.az1
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.20.0/24"

  tags = {
    Name = "subnet-test-us2a-qa-database"
  }
}

resource "aws_subnet" "subnet_database_2" {
  availability_zone = var.az2
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.21.0/24"

  tags = {
    Name = "subnet-test-us2b-qa-database"
  }
}

resource "aws_subnet" "subnet_database_3" {
  availability_zone = var.az3
  vpc_id = aws_vpc.qa-vpc.id
  cidr_block = "192.170.22.0/24"

  tags = {
    Name = "subnet-test-us2c-qa-database"
  }
}

#Subnet EFS

#resource "aws_subnet" "subnet_public_EFS" {
 # availability_zone = var.az1
  #vpc_id = aws_vpc.qa-vpc.id
  #cidr_block = "192.170.5.0/24"

  #tags = {
   # Name = "subnet-test-us2f-qa-efs"
  #}
#}



#internet gateway

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.qa-vpc.id
  tags = {
    Name = "igw-test-us2-qa"

  }
}

#NAT Gateway

resource "aws_nat_gateway" "gw" {

  allocation_id = aws_eip.eip_nat-gw.id
  subnet_id     = aws_subnet.subnet_public_1.id
  tags = {
    Name = "ngw-mef-us2-qa"
  }
}

#Route table private
resource "aws_route_table" "route_table_private" {
    vpc_id = aws_vpc.qa-vpc.id

    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.gw.id
    }
    tags = {
      Name = "rtb-test-us2-qa-private"
    }
  }

#Route table public
resource "aws_route_table" "route_table_public" {
  vpc_id = aws_vpc.qa-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name = "rtb-test-us2-qa-public"
  }
}

#Route table database
resource "aws_route_table" "route_table_database" {
  vpc_id = aws_vpc.qa-vpc.id
  tags = {
    Name = "rtb-test-us2-qa-database"
  }
}


#Route table EFS
#resource "aws_route_table" "route_table_EFS" {
#  vpc_id = aws_vpc.qa-vpc.id

 # route {
  #  cidr_block = "0.0.0.0/0"
   # gateway_id = aws_internet_gateway.gw.id
  #}
  #tags = {
  #  Name = "rtb-test-us2-qa-efs"
  #}
#}

#Route-association-private

resource "aws_route_table_association" "subnet_private_1" {
  subnet_id = aws_subnet.subnet_private_1.id
  route_table_id = aws_route_table.route_table_private.id
}

resource "aws_route_table_association" "subnet_private_2" {
  subnet_id = aws_subnet.subnet_private_2.id
  route_table_id = aws_route_table.route_table_private.id
}

resource "aws_route_table_association" "subnet_private_3" {
  subnet_id = aws_subnet.subnet_private_3.id
  route_table_id = aws_route_table.route_table_private.id
}

#Route-association-public

resource "aws_route_table_association" "subnet_public_1" {
  subnet_id = aws_subnet.subnet_public_1.id
  route_table_id = aws_route_table.route_table_public.id
}

resource "aws_route_table_association" "subnet_public_2" {
  subnet_id = aws_subnet.subnet_public_2.id
  route_table_id = aws_route_table.route_table_public.id
}

resource "aws_route_table_association" "subnet_public_3" {
  subnet_id = aws_subnet.subnet_public_3.id
  route_table_id = aws_route_table.route_table_public.id
}

#Route-association-database

resource "aws_route_table_association" "subnet_database_1" {
  subnet_id = aws_subnet.subnet_database_1.id
  route_table_id = aws_route_table.route_table_database.id
}

resource "aws_route_table_association" "subnet_database_2" {
  subnet_id = aws_subnet.subnet_database_2.id
  route_table_id = aws_route_table.route_table_database.id
}

resource "aws_route_table_association" "subnet_database_3" {
  subnet_id = aws_subnet.subnet_database_3.id
  route_table_id = aws_route_table.route_table_database.id
}

#Route-association-EFS

#resource "aws_route_table_association" "subnet_public_EFS" {
#  subnet_id = aws_subnet.subnet_public_EFS.id
 # route_table_id = aws_route_table.route_table_EFS.id
#}







