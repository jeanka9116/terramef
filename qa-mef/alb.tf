#TARGET APP
resource "aws_lb_target_group" "tg-qa-app" {
  name        = "tg-test-us2-qa-app"
  port        = 80
  protocol    = "HTTP"
  target_type =  "instance"
  vpc_id = aws_vpc.qa-vpc.id


  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

}
#TARGET WEB
resource "aws_lb_target_group" "tg-qa-web" {
  name        = "tg-test-us2-qa-web"
  port        = 80
  protocol    = "HTTP"
  target_type =  "instance"
  vpc_id = aws_vpc.qa-vpc.id

  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

}

resource "aws_lb_target_group_attachment" "alb-target-group-app" {
  target_group_arn = aws_lb_target_group.tg-qa-app.arn
  target_id        = aws_instance.qa-app.id
  port             = 80
}
resource "aws_lb_target_group_attachment" "alb-target-group-web" {
  target_group_arn = aws_lb_target_group.tg-qa-web.arn
  target_id        = aws_instance.qa-web.id
  port             = 80
}

#ALB APP
resource "aws_lb" "alb-app" {
  name     = "alb-mef-us2-qa-app"
  internal = true
  security_groups = [aws_security_group.sg-alb-app.id]
  subnets         = [aws_subnet.subnet_private_1.id, aws_subnet.subnet_private_2.id, aws_subnet.subnet_private_3.id]
  ip_address_type    = "ipv4"
  load_balancer_type = "application"
  tags = {
    Name = "alb-mef-us2-qa-app"
  }
}

resource "aws_lb_listener" "alb-app-listener" {
  load_balancer_arn = aws_lb.alb-app.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg-qa-app.arn
  }
}

#ALB WEB

resource "aws_lb" "alb-web" {
  name     = "alb-mef-us2-qa-web"
  internal = false
  security_groups     = [aws_security_group.sg-alb-pub.id]
  subnets             = [aws_subnet.subnet_public_1.id, aws_subnet.subnet_public_2.id, aws_subnet.subnet_public_3.id]
  ip_address_type     = "ipv4"
  load_balancer_type  = "application"
  tags = {
    Name = "alb-mef-us2-qa-web"
  }
}

resource "aws_lb_listener" "alb-web-listener" {
  load_balancer_arn = aws_lb.alb-web.arn
  port               = 443
  protocol           = "HTTPS"
  ssl_policy         = "ELBSecurityPolicy-2016-08"
  #certificate_arn   = aws_acm_certificate.domain_certificate.arn###cuando la acm se automatiza
  certificate_arn    = "arn:aws:acm:us-east-2:576675353010:certificate/8bbdec96-c9e5-44d7-9232-c71341f4c5d5"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg-qa-web.arn
  }
}

