/*

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "terramefqa"
}
data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.terramefqacloudfront-qa.arn}/*"]

    principals {
      type = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.terramefqacloudfront-qa.arn]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }
}


resource "aws_s3_bucket_policy" "example" {
  bucket = aws_s3_bucket.terramefqacloudfront-qa.id
  policy = data.aws_iam_policy_document.s3_policy.json
}




locals {
  s3_origin_id = "S3-www.coursetestaws.be"
}
*/

/*
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.terramefqacloudfront-qa.bucket_regional_domain_name
    origin_id   = local.s3_origin_id


    s3_origin_config {
      #origin_access_identity = "origin-access-identity/cloudfront/ABCDEFG1234567"
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  #comment             = "cloudfront-mef"
  default_root_object = "index.html"
  aliases = ["www.coursetestaws.be"]

 # logging_config {
  #  include_cookies = false
   # bucket          = "mylogs.s3.amazonaws.com"
    #prefix          = "myprefix"
  #}

  #aliases = ["mysite.example.com", "yoursite.example.com"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
  }

  # Cache behavior with precedence 0
  #ordered_cache_behavior {
   # path_pattern     = "*"
    #allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    #cached_methods   = ["GET", "HEAD", "OPTIONS"]
    #target_origin_id = local.s3_origin_id

    #forwarded_values {
    #  query_string = false
    #  headers      = ["Origin"]

    #  cookies {
    #    forward = "none"
      #}
    #}

    #min_ttl                = 0
    #default_ttl            = 0
    #max_ttl                = 0
    #compress               = true
    #viewer_protocol_policy = "redirect-to-https"
  #}




  # Cache behavior with precedence 1
  #ordered_cache_behavior {
   # path_pattern     = "/content/*"
    #allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    #cached_methods   = ["GET", "HEAD"]
    #target_origin_id = local.s3_origin_id

    #forwarded_values {
     # query_string = false

      #cookies {
      #  forward = "none"
      #}
    #}

    #min_ttl                = 0
    #default_ttl            = 0
    #max_ttl                = 0
    #compress               = true
    #viewer_protocol_policy = "redirect-to-https"
  #}

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
      #locations        = ["US", "CA", "GB", "DE", "PE"]
    }
  }

  tags = {
    Environment = "production"
  }

  viewer_certificate {
    ssl_support_method = "sni-only"
    cloudfront_default_certificate = false
    acm_certificate_arn = "arn:aws:acm:us-east-1:576675353010:certificate/b7ec884c-58f0-419b-8fe0-dec8410ee5c6"
  }
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.main.zone_id
  name = "www.coursetestaws.be"
  type = "A"

  alias {
    evaluate_target_health = false
    name = aws_cloudfront_distribution.s3_distribution.domain_name
    zone_id = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
  }
}

*/


#El siguiente ejemplo a continuación crea una distribución de Cloudfront con un grupo de origen para el enrutamiento de conmutación por error:
/*
resource "aws_cloudfront_distribution" "s3_distribution" {
origin_group {
origin_id = "groupS3"

failover_criteria {
status_codes = [403, 404, 500, 502]
}

member {
origin_id = "primaryS3"
}

member {
origin_id = "failoverS3"
}
}

origin {
domain_name = "${aws_s3_bucket.primary.bucket_regional_domain_name}"
origin_id   = "primaryS3"

s3_origin_config {
origin_access_identity = "${aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path}"
}
}

origin {
domain_name = "${aws_s3_bucket.failover.bucket_regional_domain_name}"
origin_id   = "failoverS3"

s3_origin_config {
origin_access_identity = "${aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path}"
}
}

default_cache_behavior {
# ... other configuration ...
target_origin_id = "groupS3"
}

# ... other configuration ...
}
*/