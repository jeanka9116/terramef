variable "region" {
  default = "us-east-2"
}

variable "az1" {
  default = "us-east-2a"
}

variable "az2" {
  default = "us-east-2b"
}

variable "az3" {
  default = "us-east-2c"
}


variable "vpc_cidr" {
  default = "192.170.0.0/16"
}

variable "ami" {
  default = "ami-0f7919c33c90f5b58"
}
#LAUNCH CONFIGURATION VARIABLES BASTION
variable "instance_type_bastion" {
  default = "t2.micro"
}
#LAUNCH CONFIGURATION VARIABLES WEB

variable "instance-type-web" {
  default = "t2.micro"
}
#LAUNCH CONFIGURATION VARIABLES APP

variable "instance-type-app" {
  default = "t2.micro"
}

#LAUNCH CONFIGURATION VARIABLES EFS

variable "instance-type-efs" {
  default = "t2.micro"
}
#Variable eip NAT#
variable "eip_association_ngw_qa" {
  default = "eip-ngw"
}

######################Variables rds####################################
variable "username" {
  default = "admin"
  #type        = string
}

variable "password" {
  default = "pameriano123"
  #type        = string
}

variable "database_name" {
  default = "dbadmin"
  #type        = string
}

# ---------------------------------------------------------------------------------------------------------------------
# Variables RDS
# ---------------------------------------------------------------------------------------------------------------------

variable "name" {
  description = "dbrds"
  type        = string
  default     = "db-mef-ue1-prod-global"
}

variable "engine_name" {
  description = "dbadmin"
  type        = string
  default     = "mysql"
}

variable "family" {
  description = "Family of the database"
  type        = string
  default     = "mysql5.7"
}

variable "port" {
  description = "Port which the database should run on"
  type        = number
  default     = 3306
}

variable "major_engine_version" {
  description = "MAJOR.MINOR version of the DB engine"
  type        = string
  default     = "5.7"
}

variable "engine_version" {
  description = "Version of the database to be launched"
  default     = "5.7.21"
  type        = string
}

variable "allocated_storage" {
  description = "Disk space to be allocated to the DB instance"
  type        = number
  default     = 5
}

variable "license_model" {
  description = "License model of the DB instance"
  type        = string
  default     = "general-public-license"
}



