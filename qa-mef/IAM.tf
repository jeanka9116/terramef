/*
resource "aws_iam_user" "gitlab" {
  name = "terramef-gitlab"

  tags = {
    tag-key = "terramef-gitlab"
  }
}

resource "aws_iam_access_key" "gitlab" {
  user = aws_iam_user.gitlab.name
}

resource "aws_iam_user_policy" "gitlab" {
  name = "gitlab"
  user = aws_iam_user.gitlab.name

  policy = <<EOF
{
  "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:ListBucket",
                "s3:DeleteObject",
                "cloudfront:CreateInvalidation"
            ],
            "Resource": [
                "arn:aws:cloudfront::576675353010:distribution/EOBF442TQBNNW",
                "arn:aws:s3:::www.coursetestaws.be/*",
                "arn:aws:s3:::www.coursetestaws.be"
            ]
        }
    ]
}
EOF
}
*/