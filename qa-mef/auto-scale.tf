########EJECUTAR DESPUES DE CREAR LAS INSTANCIAS####################
#Launch-template y Autoscaling WEB###########################
/*
#####AMI WEB#############
resource "aws_ami_from_instance" "img-web" {
  name               = "img_ec2-test-us2-qa-web-09-05-20-test-ultimate"
  source_instance_id = aws_instance.qa-web.id
  tags = {
    Name = "img_ec2-test-us2-qa-web-20-05-20-test-ultimate"
  }
}

resource "aws_launch_template" "launch-template-web" {
  name = "lt-test-us2-qa-web"
  image_id = aws_ami_from_instance.img-web.id
  instance_type = var.instance-type-web
  key_name = "mefqa-terraform"
  vpc_security_group_ids = [aws_security_group.sg-web.id]
}

resource "aws_autoscaling_group" "autoscaling-group-web" {
  name = "asg-test-us2-qa-web"
  max_size = 4
  min_size = 1
  health_check_grace_period = 300
  desired_capacity = 2
  force_delete = true
  vpc_zone_identifier = [aws_subnet.subnet_public_1.id, aws_subnet.subnet_public_2.id, aws_subnet.subnet_public_3.id]
  health_check_type = "ELB"
  target_group_arns = [aws_lb_target_group.tg-qa-web.arn]
  launch_template {
    id      = aws_launch_template.launch-template-web.id
    version = "$Latest"
  }

  tag  {
    key = "Name"
    propagate_at_launch = true
    value = "asg-test-us2-qa-web"
  }
}

#Launch-template y Autoscaling APP

####AMI APP##############################
resource "aws_ami_from_instance" "img-app" {
  name               = "img_ec2-test-us2-qa-app-20-05-20-ultimate"
  source_instance_id = aws_instance.qa-app.id
  tags = {
    Name = "img_ec2-test-us2-qa-app-13-05-20-test-ultimate"
  }
}

resource "aws_launch_template" "launch-template-app" {
  name = "lt-test-us2-qa-app"
  image_id = aws_ami_from_instance.img-app.id
  instance_type = var.instance-type-app
  key_name = "mefqa-terraform"
  vpc_security_group_ids = [aws_security_group.sg-app.id]
}

resource "aws_autoscaling_group" "autoscaling-group-app" {
  name = "asg-test-us2-qa-app"
  max_size = 4
  min_size = 1
  health_check_grace_period = 300
  desired_capacity = 2
  force_delete = true
  vpc_zone_identifier = [aws_subnet.subnet_private_1.id, aws_subnet.subnet_private_2.id, aws_subnet.subnet_private_3.id]
  health_check_type = "ELB"
  target_group_arns = [aws_lb_target_group.tg-qa-app.arn]

  launch_template {
    id      = aws_launch_template.launch-template-app.id
    version = "$Latest"
  }

  tag  {
    key = "Name"
    propagate_at_launch = true
    value = "asg-test-us2-qa-app"
  }
}
*/
