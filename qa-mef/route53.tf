#######REQUERIMIENTO DE ACM AUTOMATICO#############
/*resource "aws_acm_certificate" "domain_certificate" {
 domain_name       = "*.coursetestaws.be"
  validation_method = "DNS"

  tags = {
    Name = "Certificate"
  }
}
*/

data "aws_route53_zone" "main" {
  name         = "coursetestaws.be"
  private_zone = false
}

########REQUERIMIENTO DE ACM AUTOMATICO############
/*
resource "aws_route53_record" "cert_validation_record" {
  name            = aws_acm_certificate.domain_certificate.domain_validation_options[0].resource_record_name
  type            = aws_acm_certificate.domain_certificate.domain_validation_options[0].resource_record_type
  zone_id         = data.aws_route53_zone.main.id
  records         = [aws_acm_certificate.domain_certificate.domain_validation_options[0].resource_record_value]
  ttl             = 60
  allow_overwrite = true
}

resource "aws_acm_certificate_validation" "domain_certificate_validation" {
  certificate_arn         = aws_acm_certificate.domain_certificate.arn
  validation_record_fqdns = [aws_route53_record.cert_validation_record.fqdn]
}
*/
###########################################################################

resource "aws_route53_record" "lb-web" {
  name    = "test9116.coursetestaws.be"
  type    = "CNAME"
  zone_id = data.aws_route53_zone.main.zone_id
  ttl = "60"
  records = [aws_lb.alb-web.dns_name]
}