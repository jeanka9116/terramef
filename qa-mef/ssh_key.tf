resource "aws_key_pair" "qamef_terraform" {
  public_key = file("~/.ssh/mef-qa.pub")
  key_name = "mefqa-terraform"
}