resource "aws_instance" "qa-web" {
  ami = var.ami
  instance_type = var.instance-type-web
  vpc_security_group_ids = [aws_security_group.sg-web.id]
  subnet_id = aws_subnet.subnet_public_1.id
  associate_public_ip_address = true
  private_ip = "192.170.1.37"
  #root_block_device { volume_size = 20 }
  key_name = "mefqa-terraform"
  depends_on = [aws_internet_gateway.gw]
  user_data = file("script.sh")
  tags = {
    Name= "ec2-test-us2-qa-web_BASE"
    Env= "web"
  }
}


resource "aws_instance" "qa-app" {
  ami = var.ami
  instance_type = var.instance-type-app
  vpc_security_group_ids = [aws_security_group.sg-app.id]
  subnet_id = aws_subnet.subnet_private_1.id
  associate_public_ip_address = true
  private_ip = "192.170.10.38"
  #root_block_device { volume_size = 20 }
  key_name = "mefqa-terraform"
  depends_on = [aws_internet_gateway.gw]
  user_data = file("script.sh")
  tags = {
    Name= "ec2-test-us2-qa-app_BASE"
    Env= "app"
  }
}

resource "aws_instance" "qa-bastion" {
  ami = var.ami
  instance_type = var.instance_type_bastion
  vpc_security_group_ids = [aws_security_group.sg-bastion.id]
  subnet_id = aws_subnet.subnet_public_1.id
  associate_public_ip_address = true
  private_ip = "192.170.1.39"
  #root_block_device { volume_size = 20 }
  key_name = "mefqa-terraform"
  depends_on = [aws_internet_gateway.gw]
  user_data = file("script_bastion.sh")
  tags = {
    Name= "ec2-test-us2-qa-bastion_BASE"
    Env= "bastion"
  }
}


/*resource "aws_instance" "qa-EFS" {
  ami = var.ami
  instance_type = var.instance-type-efs
  vpc_security_group_ids = [aws_security_group.sg-EFS.id]
  subnet_id = aws_subnet.subnet_public_EFS.id
  associate_public_ip_address = true
  private_ip = "192.170.5.40"
  #root_block_device { volume_size = 20 }
  key_name = "mefqa-terraform"
  depends_on = [aws_internet_gateway.gw]
    user_data = file("script.sh")
  tags = {
    Name= "ec2-test-us2-qa-EFS"
    Env= "EFS"
  }
}
*/


