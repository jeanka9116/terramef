output "qa-web-eip" {
  value = aws_instance.qa-web.public_ip
}

output "qa-bastion-eip" {
  value = aws_instance.qa-bastion.public_ip
}

output "qa-app-eip" {
  value = aws_instance.qa-app.public_ip
}

output "qa-ngw-eip" {
  value = aws_eip.eip_nat-gw.public_ip
}

output "alb_dns_name_app" {
  value = aws_lb.alb-app.dns_name
}

output "alb_dns_name_web" {
  value = aws_lb.alb-web.dns_name
}

output "alb_target_app_group_arn" {
  value = aws_lb_target_group.tg-qa-app.arn
}

output "alb_target_web_group_arn" {
  value = aws_lb_target_group.tg-qa-web.arn
}

output "efs-id" {
  value = aws_efs_file_system.efs-mef_file.arn
}

#output "db_instance_id" {
 # value = aws_db_instance.rds-qa.id
#}

#output "vpc-flowlogs-mef" {
 # value = aws_s3_bucket.bucketlog-qa.arn
#}

#output "s3-bucket-mefqa" {
 # value = aws_s3_bucket.terramefqacloudfront-qa.arn
#}

#output "cloudfront-mefqa" {
#  value = aws_cloudfront_distribution.s3_distribution.domain_name
#}


##############Output Autoscaling#######################################
/*
output "launch-template-web" {
  value = aws_launch_template.launch-template-web.name
}

output "autoscaling-group-web" {
  value = aws_autoscaling_group.autoscaling-group-web.name
}

output "launch-template-app" {
  value = aws_launch_template.launch-template-app.arn
}

output "autoscaling-group-app" {
  value = aws_autoscaling_group.autoscaling-group-app.name
}
*/