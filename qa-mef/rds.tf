/*
resource "aws_db_subnet_group" "rds-qa" {
  name       = var.name
  subnet_ids = [aws_subnet.subnet_database_1.id, aws_subnet.subnet_database_2.id, aws_subnet.subnet_database_3.id]

  tags = {
    Name = "sng-mef-us2-qa-db"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A CUSTOM PARAMETER GROUP AND AN OPTION GROUP FOR CONFIGURABILITY
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_db_option_group" "rds-qa" {
  name                 = var.name
  engine_name          = var.engine_name
  major_engine_version = var.major_engine_version

  tags = {
    Name = var.name
  }

  option {
    option_name = "MARIADB_AUDIT_PLUGIN"

    option_settings {
      name  = "SERVER_AUDIT_EVENTS"
      value = "CONNECT"
    }
  }
}

resource "aws_db_parameter_group" "rds-qa" {
  name   = var.name
  family = var.family

  tags = {
    Name = var.name
  }

  parameter {
    name  = "general_log"
    value = "0"
  }
}
*/

# ---------------------------------------------------------------------------------------------------------------------
# CREATE THE DATABASE INSTANCE
# ---------------------------------------------------------------------------------------------------------------------
/*
resource "aws_db_instance" "rds-qa" {
  identifier             = var.name
  engine                 = var.engine_name
  engine_version         = var.engine_version
  port                   = var.port
  name                   = var.database_name
  username               = var.username
  password               = var.password
  instance_class         = "db.t2.micro"
  allocated_storage      = var.allocated_storage
  skip_final_snapshot    = true
  license_model          = var.license_model
  db_subnet_group_name   = aws_db_subnet_group.rds-qa.id
  vpc_security_group_ids = [aws_security_group.sg-database.id]
  publicly_accessible    = true
  parameter_group_name   = aws_db_parameter_group.rds-qa.id
  option_group_name      = aws_db_option_group.rds-qa.id

  tags = {
    Name = "db-mef-us2-prod-global"
  }
}
*/