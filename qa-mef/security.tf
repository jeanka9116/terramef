#Security group bastion
resource "aws_security_group" "sg-bastion" {
  name = "bastion"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["179.6.199.46/32", "179.6.215.46/32"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
#Security group alb-pub
resource "aws_security_group" "sg-alb-pub" {
  name = "sec-grp-test-us2-qa-alb-pub"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}


#Security group cloudflare
resource "aws_security_group" "sg-cloudflare" {
  name = "sec-grp-test-us2-cloudflare"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["173.245.48.0/20","103.21.244.0/22","103.22.200.0/22","103.31.4.0/22","108.162.192.0/18","190.93.240.0/20","188.114.96.0/20","197.234.240.0/22","198.41.128.0/17","162.158.0.0/15","172.64.0.0/13","131.0.72.0/22","141.101.64.0/18"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Security group alb-app
resource "aws_security_group" "sg-alb-app" {
  name = "sec-grp-test-us2-qa-alb-app"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = [aws_security_group.sg-web.id]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Security group web
resource "aws_security_group" "sg-web" {
  name = "sec-grp-test-us2-qa-web"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = [aws_security_group.sg-alb-pub.id]

  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [aws_security_group.sg-bastion.id]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Security group app
resource "aws_security_group" "sg-app" {
  name = "sec-grp-test-us2-qa-app"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = [aws_security_group.sg-alb-app.id, aws_security_group.sg-web.id]

  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [aws_security_group.sg-bastion.id]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Security group database
resource "aws_security_group" "sg-database" {
  name = "sec-grp-test-us2-qa-database"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    security_groups = [aws_security_group.sg-app.id, aws_security_group.sg-bastion.id]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
/*
#Security group EFS
resource "aws_security_group" "sg-EFS" {
  name = "sec-grp-test-us2-qa-efs"
  vpc_id = aws_vpc.qa-vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [aws_security_group.sg-bastion.id]
  }

  ingress {
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    security_groups = [aws_security_group.sg-web.id, aws_security_group.sg-app.id]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
*/