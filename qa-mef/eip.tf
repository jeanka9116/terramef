resource "aws_eip" "eip-qa-web" {
  vpc = true
}

resource "aws_eip" "eip-qa-bastion" {
  vpc = true
}

resource "aws_eip" "eip-qa-app" {
  vpc = true
}

#resource "aws_eip" "eip-qa-EFS" {
 # vpc = true
#}

resource "aws_eip_association" "eip_qa-web" {
  instance_id = aws_instance.qa-web.id
  allocation_id = aws_eip.eip-qa-web.id
}

resource "aws_eip_association" "eip_qa-bastion" {
  instance_id = aws_instance.qa-bastion.id
  allocation_id = aws_eip.eip-qa-bastion.id
}

resource "aws_eip_association" "eip_qa-app" {
  instance_id = aws_instance.qa-app.id
  allocation_id = aws_eip.eip-qa-app.id
}

#resource "aws_eip_association" "eip_qa-efs" {
 # instance_id = aws_instance.qa-EFS.id
  #allocation_id = aws_eip.eip-qa-EFS.id
#}

##ELASTICIP NGW##
resource "aws_eip" "eip_nat-gw" {
  vpc = true
  associate_with_private_ip = var.eip_association_ngw_qa
  tags = {
    Name = "eip-mef-us2-qa-ngw"
  }
}